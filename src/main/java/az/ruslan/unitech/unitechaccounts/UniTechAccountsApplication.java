package az.ruslan.unitech.unitechaccounts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniTechAccountsApplication {

	public static void main(String[] args) {
		SpringApplication.run(UniTechAccountsApplication.class, args);
	}

}
