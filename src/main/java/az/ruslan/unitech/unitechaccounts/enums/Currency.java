package az.ruslan.unitech.unitechaccounts.enums;

public enum Currency {
    AZN, USD, EUR, TRY;

    public static boolean isValidCurrency(String input) {
        if (input == null) {
            throw new RuntimeException("Valyuta daxil edilmeyib");
        }
        try {
            Currency.valueOf(input);
            return true;
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Daxil edien valyutaa ile emeliyyat edile bilmez");
        }
    }
}
