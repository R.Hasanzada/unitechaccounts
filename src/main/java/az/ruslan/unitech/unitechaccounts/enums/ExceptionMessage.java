package az.ruslan.unitech.unitechaccounts.enums;

import lombok.Getter;

@Getter
public enum ExceptionMessage {

    ACCOUNT_NOT_FOUND("Bu hesab: %s üzrə məlumat tapılmadı","No information found for this account: %s"),
    ACCOUNT_EXIST("Eyni hesabı daha önce yaratmısınız","You have already created the same account"),
    CUSTOMER_AND_ACCOUNT_NOT_FOUND("Bu pin: %s və hesab: %s üzrə məlumat tapılmadı","No information found for this pin: %s and account: %s"),
    INSUFFICIENT_BALANCE("Əməliyyatı yerinə yetirmək üçün kifayət qədər balans yoxdur. Balans: %s","There is not enough balance to complete the transaction. Balance: %s"),
    INACTIVE_ACCOUNT("Bu hesab: %s activ deyil","This account: %s isn't active"),
   // INACTIVE_ACCOUNT_EXIST("Eyni hesabı daha önce yaratmısınız lakin hesabınız deaktivdir","You have already created the same account but it isn't active")
    ;

    private final String messageAz;
    private final String messageEn;

    ExceptionMessage(String messageAz, String messageEn) {
        this.messageAz = messageAz;
        this.messageEn = messageEn;
    }

    public String getMessage(String language) {
        return "en".equals(language) ? messageEn : messageAz;
    }
}

