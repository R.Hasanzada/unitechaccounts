package az.ruslan.unitech.unitechaccounts.service;

import az.ruslan.unitech.unitechaccounts.dto.AccountDto;
import az.ruslan.unitech.unitechaccounts.dto.request.AccountRequestDto;
import az.ruslan.unitech.unitechaccounts.entity.Account;
import az.ruslan.unitech.unitechaccounts.enums.ExceptionMessage;
import az.ruslan.unitech.unitechaccounts.exceptions.BusinessException;
import az.ruslan.unitech.unitechaccounts.mapper.AccountMapper;
import az.ruslan.unitech.unitechaccounts.repository.AccountRepository;
import az.ruslan.unitech.unitechaccounts.util.LanguageUtil;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AccountService {

    private final AccountRepository accountRepository;
    private final LanguageUtil languageUtil;

    public AccountService(AccountRepository accountRepository, LanguageUtil languageUtil) {
        this.accountRepository = accountRepository;
        this.languageUtil = languageUtil;
    }

    public List<AccountDto> getAllAccountsByUserPin(String userPin){
        return accountRepository.findAllByUserPin(userPin)
                .stream()
                .map(AccountMapper.INSTANCE :: toDto)
                .collect(Collectors.toList());
    }

    public void addAccount(String userPin, AccountRequestDto accountRequestDto){
        checkAccountValidation(userPin, accountRequestDto.getAccountNumber());
        AccountDto accountDto = AccountDto.builder()
                .userPin(userPin)
                .accountNumber(accountRequestDto.getAccountNumber())
                .balance(BigDecimal.valueOf(0))
                .active(accountRequestDto.getActive()).build();
        accountRepository.save(AccountMapper.INSTANCE.toEntity(accountDto));
    }

    public void removeAccount(String userPin, String accountNumber){
        Account account = accountRepository
                .findAccountByUserPinAndAccountNumberAndActiveTrue(userPin, accountNumber)
                .orElseThrow(() -> new BusinessException(ExceptionMessage.ACCOUNT_NOT_FOUND.getMessage(languageUtil.getLanguage()), accountNumber));
        account.setActive(false);
        accountRepository.save(account);
    }

    public void activeAccount(String userPin, String accountNumber){
        Account account = accountRepository.findAccountByUserPinAndAccountNumberAndActiveFalse(userPin, accountNumber)
                .orElseThrow(() -> new BusinessException(ExceptionMessage.ACCOUNT_NOT_FOUND.getMessage(languageUtil.getLanguage()), accountNumber));
        account.setActive(true);
        accountRepository.save(account);
    }

    public void checkAccountValidation(String userPin, String accountNumber){
        Optional<Account> account = accountRepository.findAccountByUserPinAndAccountNumberAndActiveTrue(userPin, accountNumber);
        if (account.isPresent()) {
            if(account.get().getActive())
                throw new BusinessException(ExceptionMessage.ACCOUNT_EXIST.getMessage(languageUtil.getLanguage()));
            else
                throw new BusinessException(ExceptionMessage.INACTIVE_ACCOUNT.getMessage(languageUtil.getLanguage()), accountNumber);
        }
    }

}
