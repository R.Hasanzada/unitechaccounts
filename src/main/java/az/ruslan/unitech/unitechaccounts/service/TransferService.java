package az.ruslan.unitech.unitechaccounts.service;

import az.ruslan.unitech.unitechaccounts.client.CurrencyRestClient;
import az.ruslan.unitech.unitechaccounts.dto.AccountDto;
import az.ruslan.unitech.unitechaccounts.dto.request.TransferAmountRequestDto;
import az.ruslan.unitech.unitechaccounts.dto.request.TransferAmountDto;
import az.ruslan.unitech.unitechaccounts.entity.Account;
import az.ruslan.unitech.unitechaccounts.enums.Currency;
import az.ruslan.unitech.unitechaccounts.enums.ExceptionMessage;
import az.ruslan.unitech.unitechaccounts.exceptions.BusinessException;
import az.ruslan.unitech.unitechaccounts.mapper.AccountMapper;
import az.ruslan.unitech.unitechaccounts.repository.AccountRepository;
import az.ruslan.unitech.unitechaccounts.util.LanguageUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
public class TransferService {

    private final AccountRepository accountRepository;
    private final CurrencyRestClient currencyRestClient;
    private final LanguageUtil languageUtil;

    public TransferService(AccountRepository accountRepository, CurrencyRestClient currencyRestClient, LanguageUtil languageUtil) {
        this.accountRepository = accountRepository;
        this.currencyRestClient = currencyRestClient;
        this.languageUtil = languageUtil;
    }

    public AccountDto addBalance(String userPin, TransferAmountDto transferAmountDto) {
        Account account = ifAccountIsExistReturnAccount(userPin, transferAmountDto.getAccountNumber());
        account.setBalance(account.getBalance().add(transferAmountDto.getAmount()));
        accountRepository.save(account);
        return AccountMapper.INSTANCE.toDto(account);
    }

    public AccountDto subtractBalance(String userPin, TransferAmountDto transferAmountDto) {
        Account account = ifAccountIsExistReturnAccount(userPin, transferAmountDto.getAccountNumber());
        BigDecimal balance = account.getBalance();
        checkBalanceVerifyToTransfer(account.getBalance(), transferAmountDto.getAmount());
        account.setBalance(balance.subtract(transferAmountDto.getAmount()));
        accountRepository.save(account);
        return AccountMapper.INSTANCE.toDto(account);
    }

    public void currencyValidation(String currency) {
        Currency.isValidCurrency(currency);
    }

    @Transactional(rollbackFor = Exception.class)
    public AccountDto transferAmount(String userPin, TransferAmountRequestDto transferAmountRequestDto) {
        String accountFrom = transferAmountRequestDto.getAccountFrom();
        String accountTo = transferAmountRequestDto.getAccountTo();
        String currency = transferAmountRequestDto.getCurrency();

        BigDecimal amount = currencyIsNotAzn(currency)
                ? transferAmountRequestDto.getAmount() : convertAzn(transferAmountRequestDto.getAmount(), currency);

        Account from = ifAccountIsExistReturnAccount(userPin, transferAmountRequestDto.getAccountFrom());
        Account to = ifAccountIsExistReturnAccount(userPin, transferAmountRequestDto.getAccountTo());

        checkAccountIsActive(to);
        checkBalanceVerifyToTransfer(from.getBalance(), amount);
        BigDecimal subtractedBalance = from.getBalance().subtract(amount);
        BigDecimal addedBalance = to.getBalance().add(amount);
        from.setBalance(subtractedBalance);
        to.setBalance(addedBalance);

        return AccountMapper.INSTANCE.toDto(from);
    }

    private Account ifAccountIsExistReturnAccount(String userPin, String accountNumber){
        return accountRepository.findAccountByUserPinAndAccountNumberAndActiveTrue(userPin, accountNumber)
                .orElseThrow(() -> new BusinessException(ExceptionMessage.CUSTOMER_AND_ACCOUNT_NOT_FOUND
                        .getMessage(languageUtil.getLanguage()), userPin, accountNumber));
    }

    private void checkAccountIsActive(Account account) {
        if (!account.getActive()) {
            throw new BusinessException(ExceptionMessage.INACTIVE_ACCOUNT
                    .getMessage(languageUtil.getLanguage()) ,account.getAccountNumber());
        }
    }

    private void checkBalanceVerifyToTransfer(BigDecimal balance, BigDecimal subtractAmount) {
        if (balance.compareTo(subtractAmount) <= 0) {
            throw new BusinessException(ExceptionMessage.INSUFFICIENT_BALANCE
                    .getMessage(languageUtil.getLanguage()), balance);
        }
    }

    private BigDecimal convertAzn(BigDecimal amount, String currency) {
        return amount.multiply(currencyRestClient.getRate(currency));
    }

    private boolean currencyIsNotAzn(String currency) {
        return currency.toUpperCase().equals(Currency.AZN.name());
    }
}
