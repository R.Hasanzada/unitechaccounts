package az.ruslan.unitech.unitechaccounts.repository;

import az.ruslan.unitech.unitechaccounts.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    List<Account> findAllByUserPin(String userPin);
    Optional<Account> findAccountByUserPin(String userPin);
    Optional<Account> findAccountByUserPinAndAccountNumberAndActiveTrue(String userPin, String accountNumber);
    Optional<Account> findAccountByUserPinAndAccountNumberAndActiveFalse(String userPin, String accountNumber);
    Optional<Account> findAccountByAccountNumber(String accountNumber);
}
