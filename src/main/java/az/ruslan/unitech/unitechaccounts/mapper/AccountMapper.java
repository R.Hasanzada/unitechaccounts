package az.ruslan.unitech.unitechaccounts.mapper;

import az.ruslan.unitech.unitechaccounts.dto.AccountDto;
import az.ruslan.unitech.unitechaccounts.entity.Account;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AccountMapper {

    AccountMapper INSTANCE = Mappers.getMapper(AccountMapper.class);

    AccountDto toDto(Account account);
    Account toEntity(AccountDto accountDto);
}
