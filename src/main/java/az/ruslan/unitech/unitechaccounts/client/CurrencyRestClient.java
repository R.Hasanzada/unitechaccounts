package az.ruslan.unitech.unitechaccounts.client;

import az.ruslan.unitech.unitechaccounts.config.UrlConfig;
import az.ruslan.unitech.unitechaccounts.dto.response.RateResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class CurrencyRestClient {

    private final RestTemplate restTemplate;
    private final UrlConfig urlConfig;

    public BigDecimal getRate(String currency){
        String exchangeUrl = urlConfig.getExchangeUrl();
        String url = String.format(exchangeUrl, currency.toUpperCase());
        return restTemplate.getForEntity(url, RateResponse.class).getBody().getRate();
    }

}
