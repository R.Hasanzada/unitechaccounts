package az.ruslan.unitech.unitechaccounts.util;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LanguageUtil {

    private final HttpServletRequest request;

    public String getLanguage(){
        try {
            return getLang(request.getHeader("Accept-Language"));
        } catch (Exception ex){
            return "az";
        }
    }


    private String getLang(String lang){
        if(lang.isBlank()) return "az";
        if(List.of("az", "en").contains(lang.toLowerCase())){
            return lang.toLowerCase();
        }
        return "az";
    }
}
