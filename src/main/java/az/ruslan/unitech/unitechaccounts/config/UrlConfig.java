package az.ruslan.unitech.unitechaccounts.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class UrlConfig {

    @Value("${endpoint.exchange_rate}")
    private String exchangeUrl;

}
