package az.ruslan.unitech.unitechaccounts.dto.request;

import lombok.Data;

@Data
public class AccountRequestDto {

    private String accountNumber;
    private Boolean active;
}
