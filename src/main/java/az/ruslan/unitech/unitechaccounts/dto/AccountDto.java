package az.ruslan.unitech.unitechaccounts.dto;

import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
public class AccountDto {

    private String userPin;
    private String accountNumber;
    private BigDecimal balance;
    private Boolean active;

}
