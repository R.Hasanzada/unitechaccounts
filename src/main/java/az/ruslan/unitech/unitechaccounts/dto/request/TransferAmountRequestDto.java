package az.ruslan.unitech.unitechaccounts.dto.request;

import az.ruslan.unitech.unitechaccounts.enums.Currency;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransferAmountRequestDto {

    private BigDecimal amount;
    private String currency;
    private String accountFrom;
    private String accountTo;

}
