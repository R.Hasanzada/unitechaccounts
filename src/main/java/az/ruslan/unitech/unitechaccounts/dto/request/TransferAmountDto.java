package az.ruslan.unitech.unitechaccounts.dto.request;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransferAmountDto {

    private String accountNumber;
    private BigDecimal amount;
}
