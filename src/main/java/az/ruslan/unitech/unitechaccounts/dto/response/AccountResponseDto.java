package az.ruslan.unitech.unitechaccounts.dto.response;

import lombok.Data;

@Data
public class AccountResponseDto {

    private String userPin;
    private String account;
    private double balance;
    private Boolean active;
}
