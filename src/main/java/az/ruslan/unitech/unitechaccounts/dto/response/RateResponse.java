package az.ruslan.unitech.unitechaccounts.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
public class RateResponse implements Serializable {

    @JsonProperty("conversion_rate")
    private BigDecimal rate;
}
