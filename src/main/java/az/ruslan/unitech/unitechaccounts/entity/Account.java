package az.ruslan.unitech.unitechaccounts.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Entity
@Table(name = "accounts")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "pin")
    private String userPin;
    private String accountNumber;
    private BigDecimal balance;
    private Boolean active;

}
