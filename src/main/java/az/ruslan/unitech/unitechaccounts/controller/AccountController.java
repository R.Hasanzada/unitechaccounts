package az.ruslan.unitech.unitechaccounts.controller;

import az.ruslan.unitech.unitechaccounts.dto.request.AccountRequestDto;
import az.ruslan.unitech.unitechaccounts.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("accounts")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @GetMapping("/all")
    public ResponseEntity<?> getAllAccountByUserPin(@RequestHeader String userPin) {
        return ResponseEntity.ok().body(accountService.getAllAccountsByUserPin(userPin));
    }


    @PostMapping("add")
    public ResponseEntity<?> addAccount(@RequestHeader String userPin,
                                        @RequestBody AccountRequestDto accountRequestDto) {
        accountService.addAccount(userPin, accountRequestDto);
        return ResponseEntity.ok().body(HttpStatus.CREATED);
    }

    @PatchMapping("/remove/{accountNumber}")
    public ResponseEntity<?> removeAccount(@RequestHeader String userPin,
                                           @PathVariable String accountNumber) {
        accountService.removeAccount(userPin, accountNumber);
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/active/{accountNumber}")
    public ResponseEntity<?> activeAccount(@RequestHeader String userPin,
                                           @PathVariable String accountNumber) {
        accountService.activeAccount(userPin, accountNumber);
        return ResponseEntity.ok().build();
    }

}
