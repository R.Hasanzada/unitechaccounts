package az.ruslan.unitech.unitechaccounts.controller;

import az.ruslan.unitech.unitechaccounts.dto.request.TransferAmountRequestDto;
import az.ruslan.unitech.unitechaccounts.dto.request.TransferAmountDto;
import az.ruslan.unitech.unitechaccounts.service.TransferService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/transfer")
@RequiredArgsConstructor
public class TransferController {

    private final TransferService transferService;

    @PostMapping("/add/balance")
    public ResponseEntity<?> addBalance(@RequestHeader String userPin,
                                        @RequestBody TransferAmountDto transferAmountDto) {

        return ResponseEntity.ok().body(transferService.addBalance(userPin, transferAmountDto));
    }

    @PostMapping("/subtract/balance")
    public ResponseEntity<?> subtractBalance(@RequestHeader String userPin,
                                             @RequestBody TransferAmountDto transferAmountDto) {
        return ResponseEntity.ok().body(transferService.subtractBalance(userPin, transferAmountDto));
    }

    @PostMapping("send/amount")
    public ResponseEntity<?> transferAmount(@RequestHeader String userPin,
                                            @RequestBody TransferAmountRequestDto transferAmountRequestDto) {
        transferService.currencyValidation(transferAmountRequestDto.getCurrency());
        return ResponseEntity.ok().body(transferService.transferAmount(userPin, transferAmountRequestDto));
    }
}
