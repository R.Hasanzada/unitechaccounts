package az.ruslan.unitech.unitechaccounts.exceptions;


public class BusinessException extends RuntimeException {


    public BusinessException(String message) {
        super(String.format(message));
    }

    public BusinessException(String message, Object... args) {
        super(String.format(message, args));
    }

}
