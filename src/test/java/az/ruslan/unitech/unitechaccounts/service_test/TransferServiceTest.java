package az.ruslan.unitech.unitechaccounts.service_test;

import az.ruslan.unitech.unitechaccounts.client.CurrencyRestClient;
import az.ruslan.unitech.unitechaccounts.dto.AccountDto;
import az.ruslan.unitech.unitechaccounts.dto.request.TransferAmountDto;
import az.ruslan.unitech.unitechaccounts.dto.request.TransferAmountRequestDto;
import az.ruslan.unitech.unitechaccounts.entity.Account;
import az.ruslan.unitech.unitechaccounts.repository.AccountRepository;
import az.ruslan.unitech.unitechaccounts.service.TransferService;
import az.ruslan.unitech.unitechaccounts.util.LanguageUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.math.BigDecimal;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class TransferServiceTest {

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private CurrencyRestClient currencyRestClient;
    @Mock
    private LanguageUtil languageUtil;
    @InjectMocks
    private TransferService transferService;

    private Account account;
    private Account accountTo;
    private TransferAmountDto transferAmountDto;

    @BeforeEach
    public void setUp() {
        account = new Account();
        account.setId(1l);
        account.setUserPin("aaa");
        account.setAccountNumber("123456789");
        account.setBalance(new BigDecimal("1000"));
        account.setActive(true);

        transferAmountDto = new TransferAmountDto();
        transferAmountDto.setAccountNumber("123456789");
        transferAmountDto.setAmount(new BigDecimal("100"));

        accountTo = new Account();
        account.setId(2l);
        account.setUserPin("bbb");
        accountTo.setAccountNumber("987654321");
        accountTo.setBalance(new BigDecimal("100"));
        accountTo.setActive(true);
    }

    @Test
    public void testAddBalance() {
        when(accountRepository.findAccountByUserPinAndAccountNumber("userPin", "123456789")).thenReturn(Optional.of(account));

        AccountDto result = transferService.addBalance("userPin", transferAmountDto);

        assertEquals(new BigDecimal("1100"), account.getBalance());
        verify(accountRepository).save(account);
    }

    @Test
    public void testSubtractBalance() {
        when(accountRepository.findAccountByUserPinAndAccountNumber("userPin", "123456789")).thenReturn(Optional.of(account));

        AccountDto result = transferService.subtractBalance("userPin", transferAmountDto);

        assertEquals(new BigDecimal("900"), account.getBalance());
        verify(accountRepository).save(account);
    }

    @Test
    public void testTransferAmount() {
        when(accountRepository.findAccountByUserPinAndAccountNumber("aaa", "123456789")).thenReturn(Optional.of(account));
        when(accountRepository.findAccountByAccountNumber("987654321")).thenReturn(Optional.of(accountTo));
        when(currencyRestClient.getRate("USD")).thenReturn(new BigDecimal("1.7"));
        when(languageUtil.getLanguage()).thenReturn("en");

        TransferAmountRequestDto dto = new TransferAmountRequestDto();
        dto.setAccountFrom(account.getAccountNumber());
        dto.setAccountTo(accountTo.getAccountNumber());
        dto.setAmount(new BigDecimal("100"));
        dto.setCurrency("USD");

        AccountDto result = transferService.transferAmount("aaa", dto);

        assertEquals(new BigDecimal("830.0"), account.getBalance());
        assertEquals(new BigDecimal("270.0"), accountTo.getBalance());
    }
}

