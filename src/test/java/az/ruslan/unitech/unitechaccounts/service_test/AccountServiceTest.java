package az.ruslan.unitech.unitechaccounts.service_test;

import az.ruslan.unitech.unitechaccounts.dto.request.AccountRequestDto;
import az.ruslan.unitech.unitechaccounts.entity.Account;
import az.ruslan.unitech.unitechaccounts.exceptions.BusinessException;
import az.ruslan.unitech.unitechaccounts.repository.AccountRepository;
import az.ruslan.unitech.unitechaccounts.service.AccountService;
import az.ruslan.unitech.unitechaccounts.util.LanguageUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private LanguageUtil languageUtil;
    @InjectMocks
    private AccountService accountService;

    @Test
    public void testAddAccount() {
        AccountRequestDto accountRequestDto = new AccountRequestDto();
        accountRequestDto.setAccountNumber("123456789");
        accountRequestDto.setActive(true);
        accountService.addAccount("12345", accountRequestDto);
        verify(accountRepository, times(1)).save(any(Account.class));
    }

    @Test
    public void testRemoveAccount() {
        Account account = new Account();
        account.setUserPin("12345");
        account.setAccountNumber("123456789");
        when(accountRepository.findAccountByUserPinAndAccountNumber("12345", "123456789")).thenReturn(Optional.of(account));

        accountService.removeAccount("12345", "123456789");

        assertFalse(account.getActive());
        verify(accountRepository, times(1)).save(account);
    }

    @Test
    public void testRemoveAccount_NotFound() {
        when(accountRepository.findAccountByUserPinAndAccountNumber("12345", "987654321")).thenReturn(Optional.empty());
        when(languageUtil.getLanguage()).thenReturn("EN");
        when(accountRepository.save(any())).thenThrow(new BusinessException("Account not found"));

        Exception exception = assertThrows(BusinessException.class, () -> {
            accountService.removeAccount("12345", "987654321");
        });

        assertEquals("Account not found", exception.getMessage());
    }

    @Test
    public void testActiveAccount() {
        Account account = new Account();
        account.setUserPin("12345");
        account.setAccountNumber("123456789");
        when(accountRepository.findAccountByUserPinAndAccountNumber("12345", "123456789")).thenReturn(Optional.of(account));

        accountService.activeAccount("12345", "123456789");

        assertTrue(account.getActive());
        verify(accountRepository, times(1)).save(account);
    }
}
